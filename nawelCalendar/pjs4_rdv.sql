-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 01 Avril 2018 à 18:26
-- Version du serveur :  5.7.11
-- Version de PHP :  7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pjs4_rdv`
--

-- --------------------------------------------------------

--
-- Structure de la table `rdv`
--

CREATE TABLE `rdv` (
  `dateRdv` varchar(30) NOT NULL,
  `heureRdv` varchar(10) NOT NULL,
  `disponible` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rdv`
--

INSERT INTO `rdv` (`dateRdv`, `heureRdv`, `disponible`) VALUES
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '9h - 10h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('JEUDI29-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '15h - 16h', 0),
('SAMEDI31-03-2018', '16h - 17h', 0),
('LUNDI26-03-2018', '12h - 13h', 0),
('LUNDI26-03-2018', '12h - 13h', 0),
('LUNDI26-03-2018', '12h - 13h', 0),
('LUNDI26-03-2018', '9h - 10h', 0),
('SAMEDI31-03-2018', '13h - 14h', 0),
('SAMEDI31-03-2018', '13h - 14h', 0),
('SAMEDI31-03-2018', '13h - 14h', 0),
('MARDI27-03-2018', '12h - 13h', 0),
('MARDI27-03-2018', '12h - 13h', 0),
('MARDI27-03-2018', '12h - 13h', 0),
('MARDI27-03-2018', '12h - 13h', 0),
('MARDI27-03-2018', '12h - 13h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('MERCREDI28-03-2018', '11h - 12h', 0),
('MERCREDI28-03-2018', '10h - 11h', 0),
('MERCREDI28-03-2018', '10h - 11h', 0),
('MERCREDI28-03-2018', '10h - 11h', 0),
('MERCREDI28-03-2018', '10h - 11h', 0),
('MERCREDI28-03-2018', '10h - 11h', 0),
('MERCREDI28-03-2018', '10h - 11h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '8h - 9h', 0),
('MARDI27-03-2018', '9h - 10h', 0),
('MARDI27-03-2018', '9h - 10h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('MERCREDI28-03-2018', '11h - 12h', 0),
('MERCREDI28-03-2018', '11h - 12h', 0),
('MERCREDI28-03-2018', '11h - 12h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('MERCREDI28-03-2018', '12h - 13h', 0),
('LUNDI26-03-2018', '8h - 9h', 0),
('LUNDI26-03-2018', '8h - 9h', 0);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `IdUtil` int(10) NOT NULL,
  `login` varchar(10) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `mail` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`IdUtil`, `login`, `nom`, `prenom`, `mail`) VALUES
(1, 'nawel', 'naw', 'el', 'nawel@gmail.com');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`IdUtil`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
