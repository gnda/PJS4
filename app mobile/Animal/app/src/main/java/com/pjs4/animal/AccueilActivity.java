package com.pjs4.animal;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AccueilActivity extends AppCompatActivity {

	private Button btnGetJson;
	private TextView  textViewGetJson;
	private ImageView imageLoaded;
	private ImageView img_dir_gauche_top;
	private ImageView img_dir_droite_top;
	private ImageView img_dir_gauche_vote;
	private ImageView img_dir_droite_vote;
	private JSONArray parentArray;
	private int cptTop ;
	private int cptAccueil;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_accueil);
		cptTop=0;
		cptAccueil=0;


		btnGetJson = (Button)findViewById(R.id.getJson);
		textViewGetJson = (TextView) findViewById(R.id.textJson);

		btnGetJson.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				++cptTop;
				String s = "[{\n" +
						"\t\"id_animal\": 1,\n" +
						"\t\"photo\": \"https://www.economie-magazine.com/images/dossiers/2017-06/animaux-133014.jpg\",\n" +
						"\t\"prenom\": \"Milou\",\n" +
						"\t\"espece\": \"Chien\",\n" +
						"\t\"race\": \"Race1\",\n" +
						"\t\"adresse\": \"18 all\\u00e9e de fontainebleau 75019\",\n" +
						"\t\"date_inscription\": \"2018-04-04T13:22:04+02:00\",\n" +
						"\t\"est_adopte\": false\n" +
						"}, {\n" +
						"\t\"id_animal\": 2,\n" +
						"\t\"photo\": \"http://animaux-passion.fr/wp-content/uploads/2016/03/7629168954_6ee2e3835f_z-640x420.jpg\",\n" +
						"\t\"prenom\": \"F\\u00e9lix\",\n" +
						"\t\"espece\": \"Chat\",\n" +
						"\t\"race\": \"Race1\",\n" +
						"\t\"adresse\": \"164 avenue jean jaur\\u00e8s 93500 pantin\",\n" +
						"\t\"date_inscription\": \"2018-04-04T13:22:04+02:00\",\n" +
						"\t\"est_adopte\": false\n" +
						"}, {\n" +
						"\t\"id_animal\": 3,\n" +
						"\t\"photo\": \"http://aguepine.a.g.pic.centerblog.net/135ed790.jpg\",\n" +
						"\t\"prenom\": \"Patrick\",\n" +
						"\t\"espece\": \"Iguane\",\n" +
						"\t\"race\": \"Race1\",\n" +
						"\t\"adresse\": \"17 rue rivoli 75019 paris\",\n" +
						"\t\"date_inscription\": \"2018-04-04T13:22:04+02:00\",\n" +
						"\t\"est_adopte\": false\n" +
						"}]";
				String lienAnimal3 ="http://192.168.43.223/PJS4/site-dev/public/animaux/mobile";
				String lienAnimal2 ="http://ksolid.com/ksolid/web/animal_json.txt" ;
				String lienAnimal ="http://172.20.10.5.xip.io/PJS4/site-dev/public/animaux/mobile" ;
				String lienTest="https://jsonparsingdemo-cec5b.firebaseapp.com/jsonData/moviesDemoItem.txt";
				try {
					convertStringToJSON(s);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});

		String urlImage = "https://cdn.pixabay.com/photo/2017/11/06/18/39/apple-2924531_960_720.jpg";
		imageLoaded = (ImageView) findViewById(R.id.imgload);
		Picasso.with(this).load(urlImage).into(imageLoaded);
	}

	class JSONTask extends AsyncTask<String,String,String>
	{
		@Override
		protected String doInBackground(String... params) {

			HttpURLConnection connection = null;
			BufferedReader reader = null;

			try
			{
				URL url = new URL(params[0]);
				connection = (HttpURLConnection) url.openConnection(); // ou HttpURsConnection pour la securite
				connection.connect();
				InputStream stream = connection.getInputStream();
				reader = new BufferedReader(new InputStreamReader(stream));

				StringBuffer strBuffer = new StringBuffer();

				String line = "";

				while((line = reader.readLine()) != null)
				{
					Log.i("AAAAAAAAAAA",line);
					strBuffer.append(line);
				}

				String jsonText = strBuffer.toString();
				parentArray = new JSONArray(jsonText);
			}
			catch (MalformedURLException e){e.printStackTrace();}
			catch (IOException e){e.printStackTrace();} catch (JSONException e) {
				e.printStackTrace();
			} finally
			{
				if(connection != null)
					connection.disconnect();
				try
				{
					if(reader != null)
						reader.close();
				}catch (IOException e){e.printStackTrace();}
			}


			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			//String jsonText = strJson;
			//parentObject.get;
			/*JSONArray parentArray= null;
			try {
				parentArray = parentObject.getJSONArray("0");
			} catch (JSONException e) {
				e.printStackTrace();
			}*/

			JSONObject finalObject = null;
			try {
				finalObject = parentArray.getJSONObject(0);
			} catch (JSONException e) {
				e.printStackTrace();
			}
            //textViewGetJson.setText(parentObject.toString() );

			String prenom = null;
			try {
				prenom = finalObject.getString("prenom");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			String espece = null;
			try {
				espece = finalObject.getString("espece");
			} catch (JSONException e) {
				e.printStackTrace();
			}
            String race = null;
            try {
                race = finalObject.getString("race");
            } catch (JSONException e) {
                e.printStackTrace();
            }
			String photo = null;
			try {
				photo = finalObject.getString("photo");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			textViewGetJson.setText(prenom + " - " + espece );
			//int year = finalObject.getInt("year");

			//String imageURL = "https://cdn.pixabay.com/photo/2017/11/06/18/39/apple-2924531_960_720.jpg";
			imageLoaded = (ImageView) findViewById(R.id.imgload);
			Picasso.with(getApplicationContext()).load("http://192.168.43.223/PJS4/site-dev/public/uploads/animaux/"+photo).into(imageLoaded);
		}
	}

	/*
	public void convertirJSON(String)
	{

	}*/
	public void loadImageFromURL(View view)
	{

	}

	public void convertStringToJSON(String lienAnimal2 ) throws JSONException {
		parentArray = new JSONArray(lienAnimal2);
		getAnimalData(imageLoaded,++cptTop);


	}


	public void getAnimalData(ImageView imgViewAchanger, int cpt)
	{

		JSONObject finalObject = null;
		try
		{
			finalObject = parentArray.getJSONObject(cpt);
		} catch (JSONException e) {e.printStackTrace(); }
		//textViewGetJson.setText(parentObject.toString() );

		String prenom = null;
		try
		{
			prenom = finalObject.getString("prenom");
		} catch (JSONException e) { e.printStackTrace(); }

		String espece = null;
		try
		{
			espece = finalObject.getString("espece");
		} catch (JSONException e) { e.printStackTrace(); }

		String race = null;
		try
		{
			race = finalObject.getString("race");
		} catch (JSONException e) { e.printStackTrace(); }

		String photo = null;
		try {
			photo = finalObject.getString("photo");
		} catch (JSONException e) { e.printStackTrace(); }

		textViewGetJson.setText(prenom + " - " + espece );
		//int year = finalObject.getInt("year");

		//String imageURL = "https://cdn.pixabay.com/photo/2017/11/06/18/39/apple-2924531_960_720.jpg";
		String dossier="http://192.168.43.223/PJS4/site-dev/public/uploads/animaux/";

		Picasso.with(this).load(photo).into(imgViewAchanger);
	}

	public void afficherProfileAnimal(int id)
	{

	}

	public void afficherProfileUser(int id)
	{

	}

	public void parcourirEspece(String espece)
	{
		/*
		"\t\"id_animal\": 1,\n" +
				"\t\"photo\": \"https://www.economie-magazine.com/images/dossiers/2017-06/animaux-133014.jpg\",\n" +
				"\t\"prenom\": \"Milou\",\n" +
				"\t\"espece\": \"Chien\",\n" +
				"\t\"race\": \"Race1\",\n" +
				"\t\"adresse\": \"18 all\\u00e9e de fontainebleau 75019\",\n" +
				"\t\"date_inscription\": \"2018-04-04T13:22:04+02:00\",\n" +
				"\t\"est_adopte\": false\n";*/
	}


}