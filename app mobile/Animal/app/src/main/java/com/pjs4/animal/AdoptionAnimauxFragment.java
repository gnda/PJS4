package com.pjs4.animal;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdoptionAnimauxFragment extends Fragment {

	private Button btnAdopter;
	private ImageView imgChoix;
	private TextView prenomAnim;


	public AdoptionAnimauxFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		 View view = inflater.inflate(R.layout.fragment_adoption_animaux, container, false);

		 btnAdopter = (Button) view.findViewById(R.id.btnAdopter);
		 imgChoix = (ImageView) view.findViewById(R.id.img_choix);
		 prenomAnim =(TextView) view.findViewById(R.id.nom_animal);

		  return view;
	}

	public Button getBtnAdopter() {
		return btnAdopter;
	}

	public void setBtnAdopter(Button btnAdopter) {
		this.btnAdopter = btnAdopter;
	}

	public ImageView getImgChoix() {
		return imgChoix;
	}

	public void setImgChoix(String path) {
		Picasso.with(this.getContext()).load(path).into(imgChoix);
	}

	public TextView getPrenomAnim() {
		return prenomAnim;
	}

	public void setPrenomAnim(String prenomAnim) {
		this.prenomAnim.setText(prenomAnim);
	}
}