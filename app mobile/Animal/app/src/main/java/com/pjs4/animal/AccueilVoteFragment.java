package com.pjs4.animal;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccueilVoteFragment extends Fragment {

	private View view;
	private ImageView imgVote;
	private ImageView imgtop;
	private Button btnVote;

	public AccueilVoteFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		 view  = inflater.inflate(R.layout.fragment_accueil_vote, container, false);
		 imgVote = (ImageView)view.findViewById(R.id.img_vote);
		 imgtop = (ImageView)view.findViewById(R.id.img_choix);
		 btnVote = (Button) view.findViewById(R.id.btnVote);
		 return view;
	}

	public void setImgVote(String path)
	{
		Picasso.with(getContext()).load(path).into(imgVote);
	}

	public void setImgTop(String path)
	{
		Picasso.with(getContext()).load(path).into(imgtop);
	}
}
