package com.pjs4.animal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity
		implements NavigationView.OnNavigationItemSelectedListener {
	NavigationView navigationView = null;
	private Fragment fragment;
	private FragmentTransaction fragmentTransaction;
	private JSONArray parentArray;
	private int cptTop ;
	private int cptVote;
	private int cptChoixAdoption;
	private ImageView imgTop;
	private ImageView imgVote;
	private String s = "[{\n" +
			"\t\"id_animal\": 1,\n" +
			"\t\"photo\": \"https://www.economie-magazine.com/images/dossiers/2017-06/animaux-133014.jpg\",\n" +
			"\t\"prenom\": \"Milou\",\n" +
			"\t\"espece\": \"Chien\",\n" +
			"\t\"race\": \"Race1\",\n" +
			"\t\"adresse\": \"18 all\\u00e9e de fontainebleau 75019\",\n" +
			"\t\"date_inscription\": \"2018-04-04T13:22:04+02:00\",\n" +
			"\t\"est_adopte\": false\n" +
			"}, {\n" +
			"\t\"id_animal\": 2,\n" +
			"\t\"photo\": \"http://animaux-passion.fr/wp-content/uploads/2016/03/7629168954_6ee2e3835f_z-640x420.jpg\",\n" +
			"\t\"prenom\": \"F\\u00e9lix\",\n" +
			"\t\"espece\": \"Chat\",\n" +
			"\t\"race\": \"Race1\",\n" +
			"\t\"adresse\": \"164 avenue jean jaur\\u00e8s 93500 pantin\",\n" +
			"\t\"date_inscription\": \"2018-04-04T13:22:04+02:00\",\n" +
			"\t\"est_adopte\": false\n" +
			"}, {\n" +
			"\t\"id_animal\": 3,\n" +
			"\t\"photo\": \"http://aguepine.a.g.pic.centerblog.net/135ed790.jpg\",\n" +
			"\t\"prenom\": \"Patrick\",\n" +
			"\t\"espece\": \"Iguane\",\n" +
			"\t\"race\": \"Race1\",\n" +
			"\t\"adresse\": \"17 rue rivoli 75019 paris\",\n" +
			"\t\"date_inscription\": \"2018-04-04T13:22:04+02:00\",\n" +
			"\t\"est_adopte\": false\n" +
			"}]";

	// set the fragment initially
	Toolbar toolbar = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		changerFragment(new MainFragment());

/*
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
*/
		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		});

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
	}

	public void about(View view) {                   changerFragment(new AccueilAboutFragment()); }
	public void voterSemaine(View view) {fragment = new AccueilVoteFragment();changerFragment(fragment); }
	public void voirAnimaux(View view) {             changerFragment(new AdoptionAnimauxFragment()); }
	public void afficherProfileAnimal(View view) {   changerFragment(new ProfileAnimalFragment()); }
	public void adopter(View view) {                 changerFragment(new QuestionnaireFragment()); }
	public void detaillerFicheAnimal(View view) {    changerFragment(new FicheAnimalDetailFragment()); }
	public void afficherrFicheAnimaux(View view) {   changerFragment(new FichesFragment()); }
	public void afficherrFicheSecurisme(View view) { changerFragment(new SecourismeFragment()); }
	public void affichierDetailsSecu(View view) {    changerFragment(new SecurismeDetailsFragment()); }
	@Override
	public void onBackPressed() {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("StatementWithEmptyBody")
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		// Handle navigation view item clicks here.
		int id = item.getItemId();

		if (id == R.id.accueil)
		{
			try {
				parentArray = new JSONArray(s);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			changerFragment(new AcceuilFragment());
		} else if (id == R.id.adoption)
		{
			changerFragment(new  AdoptionFragment());

		} else if (id == R.id.fiches)
		{
			changerFragment(new FichesFragment());
		} else if (id == R.id.faq)
		{
			changerFragment(new FAQFragment());
		} else if (id == R.id.forum)
		{
			//Uri uri = Uri.parse("https://www.ksolid.com");

	 		Intent intent = new Intent(this, ForumActivity.class);
	  		startActivity(intent);

		} else if (id == R.id.chat)

		{
			Intent i = new Intent(this, AccueilActivity.class);
			startActivity(i);
		}



		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}

	/*
	public void function about(View view)
	{
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(R.id.fragment_accueil,new ContactsFragment());
		fragmentTransaction.commit();
	}
	*/

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public Fragment getFragment() {
		return fragment;
	}

	public void changerFragment(Fragment newfragment)
	{
		if(getSupportFragmentManager().findFragmentById(R.id.fragment_container) != null) {
			getSupportFragmentManager()
					.beginTransaction().
					remove(getSupportFragmentManager().findFragmentById(R.id.fragment_container)).commit();
		}

		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.fragment_container, newfragment)
				.commit();
	}






	public void convertStringToJSON(String lienAnimal2 ) throws JSONException {
		//getAnimalData(imageLoaded,++cptTop);


	}


	public void getAnimalData(int type, int cpt)
	{

		JSONObject finalObject = null;
		try
		{
			finalObject = parentArray.getJSONObject(cpt);
		} catch (JSONException e) {e.printStackTrace(); }
		//textViewGetJson.setText(parentObject.toString() );

		String prenom = null;
		try
		{
			prenom = finalObject.getString("prenom");
		} catch (JSONException e) { e.printStackTrace(); }

		String espece = null;
		try
		{
			espece = finalObject.getString("espece");
		} catch (JSONException e) { e.printStackTrace(); }

		String race = null;
		try
		{
			race = finalObject.getString("race");
		} catch (JSONException e) { e.printStackTrace(); }

		String photo = null;
		try {
			photo = finalObject.getString("photo");
		} catch (JSONException e) { e.printStackTrace(); }

		//textViewGetJson.setText(prenom + " - " + espece );

		//String imageURL = "https://cdn.pixabay.com/photo/2017/11/06/18/39/apple-2924531_960_720.jpg";
		String dossier="http://192.168.43.223/PJS4/site-dev/public/uploads/animaux/";

		AccueilVoteFragment frag = (AccueilVoteFragment) fragment;
		if(type==1)
		frag.setImgTop(photo);
		if(type==2)
			frag.setImgVote(photo);

	}

	public void reculerGaucheVote(View view) throws JSONException {
		parentArray = new JSONArray(s);

		if(cptVote < parentArray.length())
		{
			cptVote--;
			if(cptVote <0) cptVote =0;
			getAnimalData(2,cptVote);
		}
	}

	public void reculerDroiteVote(View view) throws JSONException {
		parentArray = new JSONArray(s);
		if(cptVote < parentArray.length())
		{
			cptVote++;
			if(cptVote == parentArray.length()) cptVote = parentArray.length() -1;
			getAnimalData(2,cptVote);
		}
	}


	public void reculerGaucheTop(View view) throws JSONException {
		parentArray = new JSONArray(s);

		if(cptTop < parentArray.length())
		{
			cptTop--;
			if(cptTop <0) cptTop =0;
			getAnimalData(1,cptTop);
		}
	}

	public void reculerDroiteTop(View view) throws JSONException {
		parentArray = new JSONArray(s);
		if(cptTop < parentArray.length())
		{
			cptTop++;
			if(cptTop == parentArray.length()) cptTop = parentArray.length() -1;
			getAnimalData(1,cptTop);
		}
	}



	/* -------------------------------------------------------------
	  Trier les animaux
	 */

	public void getAnimal(String especeCherche) throws JSONException {
		boolean trouve = false;
		parentArray = new JSONArray(s);
		for(int i = cptChoixAdoption; i<parentArray.length() && trouve == false; i++)
		{

			JSONObject finalObject = null;
			try
			{

				finalObject = parentArray.getJSONObject(i);
				String espece = finalObject.getString("espece");
				if(finalObject.getString("espece")!= null)
				{
					if (especeCherche.equals(espece))
					{
						String prenom = finalObject.getString("prenom");
						String photo  = finalObject.getString("photo");
						int id_animal = finalObject.getInt("id_animal");
						cptChoixAdoption = i;
						trouve = false;
					}
				}

			} catch (JSONException e) { e.printStackTrace(); }
/*
			String id_animal ="";
			String prenom ="";
			String  race ="";
			String adresse ="";
			String date_Inscription="";
			String estAdopte="";

			*/
		}

/*
		AccueilVoteFragment frag = (AccueilVoteFragment) fragment;
		if(type==1)
			frag.setImgTop(photo);
		if(type==2)
			frag.setImgVote(photo);
*/
	}


	public void adopter1(View view) throws JSONException {
		getAnimal("Chien");
	}
}
