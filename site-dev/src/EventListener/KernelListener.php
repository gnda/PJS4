<?php
// src/EventListener/KernelListener.php
namespace App\EventListener;

use App\Chat\ChatLoader;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;


class KernelListener
{
    private $chat;

    public function __construct(ChatLoader $chat)
    {
        $this->chat = $chat;
    }

    public function onKernelRequest(GetResponseEvent $responseEvent)
    {
        if (!$this->chat->isLoaded()) {
            $this->chat->initIoServer();
            echo "démarré";
            die();
        }else{
            echo "arrêté";
            die();
        }
    }

}