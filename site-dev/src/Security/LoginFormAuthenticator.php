<?php

namespace App\Security;

use App\Form\LoginFormType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;
	private $formFactory;
	private $em;
	private $router;
	
    public function __construct(FormFactoryInterface $formFactory, ObjectManager $em, RouterInterface $router)
    {
        $this->formFactory = $formFactory;
		$this->em = $em;
		$this->router = $router;
    }
	
    public function getCredentials(Request $request)
    {
        $form = $this->formFactory->create(LoginFormType::class);
        $form->handleRequest($request);

        $data = $form->getData();
        $metadata = $this->em->getClassMetaData(get_class($data));
        $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $request->getSession()->set(Security::LAST_USERNAME,$data->getUsername());
        return ['_username' => $data->getUsername(), '_password' => $data->getPassword()];
    }
	
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if(isset($credentials['_username'])){
            $username = $credentials['_username'];
            //$user = $this->em->getRepository('App:Utilisateur')->findOneBy(['email' => $username]);
            //if(count($errors) > 0 || !isset($user))
            $user = $this->em->getRepository('App:Utilisateur')->findOneBy(['pseudo' => $username]);
            //var_dump($user->getIdUtilisateur());
            //die();
            if(!isset($user))
                throw new \Exception('Saisie invalide!');
            return $user;
        }
    }
	
    public function checkCredentials($credentials, UserInterface $user)
    {
		$password = $credentials['_password'];
		if ($password == $user->getPassword())
			return true;
		return false;
    }
	
	protected function getLoginUrl()
    {
		$this->router->generate('login');
    }
	
	public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        //return $token;
        //$user = $token->getUser();
        /*$request->getSession()->set("user",array(
            '_id' => $user->getIdUtilisateur(),
            '_username' => $user->getUsername()
        ));*/
        /*$request->getSession()->set('_username',$user->getUsername());*/
        $targetPath = $this->router->generate('accueil');
        //return new RedirectResponse($targetPath);
        /*$providerKey = "aaaaaa";
        return new PreAuthenticatedToken(
        $token->getUser(),
        null,
        $providerKey,
        $token->getUser()->getRoles());*/
        //$user = $token->getUser();
        //$token->setUser($user);
        //var_dump($token);
        //return new Response($token);
	}

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new RedirectResponse($this->router->generate('login'), 301);
    }

    protected function getDefaultSuccessRedirectUrl()
    {
        return $this->router->generate('accueil');
    }
	
	public function start(Request $request, AuthenticationException $authException = null){
		return new RedirectResponse($this->router->generate('login'), 301);
	}
	
	public function supports(Request $request){
        return $request->getPathInfo() == '/login' && $request->isMethod('POST');
	}

}
