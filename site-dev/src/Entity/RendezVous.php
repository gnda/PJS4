<?php

// src/Entity/RendezVous.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RendezVousRepository")
 */
class RendezVous
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id_rdv;

	/**
     * @ORM\Column(type="string", name="date_rdv")
     */
    private $date_rdv;

    /**
     * @ORM\Column(type="string", name="horaire_rdv")
     */
    private $horaire_rdv;

	/**
     * @ORM\Column(name="est_disponible", type="boolean")
     */
    private $estDisponible;

    /**
     * @ORM\OneToOne(targetEntity="Utilisateur")
     * @ORM\Column(type="integer")
     */
    private $id_utilisateur;

    /**
     * @return mixed
     */
    public function getId_Rdv()
    {
        return $this->id_rdv;
    }

    /**
     * @param mixed $id_rdv
     */
    public function setId_Rdv($id_rdv): void
    {
        $this->id_rdv = $id_rdv;
    }

    /**
     * @return mixed
     */
    public function getDate_Rdv()
    {
        return $this->date_rdv;
    }

    /**
     * @param mixed $date
     */
    public function setDate_Rdv($date): void
    {
        $this->date_rdv = $date;
    }

    /**
     * @return mixed
     */
    public function getHoraire_Rdv()
    {
        return $this->horaire_rdv;
    }

    /**
     * @param mixed $horaire
     */
    public function setHoraire_Rdv($horaire): void
    {
        $this->horaire_rdv = $horaire;
    }

    /**
     * @return mixed
     */
    public function getEstDisponible()
    {
        return $this->estDisponible;
    }

    /**
     * @param mixed $bool
     */
    public function setEstDisponible($bool): void
    {
        $this->estDisponible = $bool;
    }

    /**
     * @return mixed
     */
    public function getId_Utilisateur()
    {
        return $this->id_utilisateur;
    }

    /**
     * @param mixed $id
     */
    public function setId_Utilisateur($id): void
    {
        $this->id_utilisateur = $id;
    }
}