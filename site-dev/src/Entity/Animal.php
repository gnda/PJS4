<?php

// src/Entity/Animal.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnimalRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Animal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id_animal;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\Choice({"Chien", "Chat", "Reptile", "Oiseau", "Rongeur"})
     */
    private $espece;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $race;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $adresse;

    /**
     * @ORM\Column(type="datetime", name="date_inscription")
     */
    private $date_inscription;

    /**
     * @ORM\Column(name="est_adopte", type="boolean")
     */
    private $estAdopte;

    /**
     * @return mixed
     */
    public function getId_Animal()
    {
        return $this->id_animal;
    }

    /**
     * @param mixed $id_animal
     */
    public function setId_Animal($id_animal): void
    {
        $this->id_animal = $id_animal;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getEspece()
    {
        return $this->espece;
    }

    /**
     * @param mixed $espece
     */
    public function setEspece($espece): void
    {
        $this->espece = $espece;
    }

    /**
     * @return mixed
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @param mixed $race
     */
    public function setRace($race): void
    {
        $this->race = $race;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getDate_Inscription()
    {
        return $this->date_inscription;
    }

    /**
     * @param mixed $date_inscription
     */
    public function setDate_Inscription()
    {
        $this->date_inscription = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getEstAdopte()
    {
        return $this->estAdopte;
    }

    /**
     * @param mixed $estAdopte
     */
    public function setEstAdopte($estAdopte): void
    {
        $this->estAdopte = $estAdopte;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo): void
    {
        $this->photo = $photo;
    }
}
