<?php

// src/Entity/Utilisateur.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UtilisateurRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Utilisateur implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id_utilisateur;

    /**
     * @ORM\Column(type="string")
     */
    private $token;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    private $avatar;
	
	/**
     * @ORM\Column(type="integer")
     */
    private $type_utilisateur;
	
	/**
     * @ORM\Column(type="string", length=20, unique=TRUE)
	 * @Assert\NotBlank()
     */
    private $pseudo;
	
    /**
     * @ORM\Column(type="string", length=30)
	 * @Assert\NotBlank()
     */
    private $mdp;
	
	/**
     * @var string
     */
	private $plainPassword;

	/**
     * @ORM\Column(type="string", length=30)
     */
    private $prenom;
	
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $nom;
	
	/**
     * @ORM\Column(type="string", length=40)
     */
    private $adresse;
	
	/**
     * @ORM\Column(type="string", length=40, unique=TRUE)
     * @Assert\Email()
     */
    private $email;
	
	/**
     * @ORM\Column(type="datetime", name="date_inscription")
     */
    private $date_inscription;
	
	/**
     * @ORM\Column(name="est_actif", type="boolean")
     */
    private $estActif;

    /**
     * @ORM\Column(name="roles", type="array")
     */
    private $roles = array();

    public function setIdUtilisateur($id_utilisateur)
    {
        $this->id_utilisateur = $id_utilisateur;
    }
    
    public function getIdUtilisateur()
    {
        return $this->id_utilisateur;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }
    
    public function getToken()
    {
        return $this->token;
    }
	
	public function setTypeUtilisateur($type_utilisateur)
    {
        $this->type_utilisateur = $type_utilisateur;
    }
	
	public function getTypeUtilisateur()
    {
        return $this->type_utilisateur;
    }
	
	public function setUsername($pseudo)
    {
		$this->pseudo = $pseudo;
    }
	
	public function getUsername()
    {
		return $this->pseudo;
    }
	
	public function setEmail($email)
    {
		$this->email = $email;
    }
	
	public function getEmail()
    {
		return $this->email;
    }
	
	public function setPassword($mdp)
    {
        $this->mdp = $mdp;
    }

    public function getPassword()
    {
        return $this->mdp;
    }
	
	public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }
	
	public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getNom()
    {
        return $this->nom;
    }
	
	public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    public function getAdresse()
    {
        return $this->adresse;
    }
	
	public function setDate_Inscription()
    {
        $this->date_inscription = new \DateTime('now');
    }

    public function getDate_Inscription()
    {
        return $this->date_inscription;
    }

	public function getPlainPassword()
	{
		return $this->plainPassword;
	}

	public function setPlainPassword($plainPassword)
	{
		$this->plainPassword = $plainPassword;
		$this->mdp = null;
	}
	
	public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function setRoles($roles)
    {
        $this->roles=$roles;
    }
	
    public function getRoles()
    {
		if($this->type_utilisateur == 1)
			return array('ROLE_ADMIN');
		elseif($this->type_utilisateur == 2)
			return array('ROLE_MODERATOR');
		else
            return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
		$this->plainPassword = null;
    }
	
	public function isAccountNonExpired()
	{
		return true;
	}

	public function isAccountNonLocked()
	{
		return true;
	}

	public function isCredentialsNonExpired()
	{
		return true;
	}
	
	public function setEnabled($actif)
	{
		$this->estActif = $actif;
	}

	public function isEnabled()
	{
		return $this->estActif;
	}

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar): void
    {
        $this->avatar = $avatar;
    }

    public function serialize() {
        return serialize(array($this->id_utilisateur, $this->pseudo, $this->mdp, $this->estActif));
    }

    public function unserialize($data) {
        list ($this->id_utilisateur, $this->pseudo, $this->mdp, $this->estActif) = unserialize($data);
    }
	
}
