<?php

namespace App\Repository;

use App\Entity\RendezVous;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class RendezVousRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RendezVous::class);
    }

    public function getRdvUser($id_user)
    {
        return $this->createQueryBuilder('r')
            ->where('r.id_utilisateur = :id_utilisateur')
            ->setParameter('id_utilisateur', $id_user)
            ->getQuery()
            ->getArrayResult();
    }
}