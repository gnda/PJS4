<?php

namespace App\Repository;

use App\Entity\Animal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AnimalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Animal::class);
    }

	public function getAnimal($id)
    {
        return $this->createQueryBuilder('a')
            ->where('a.id_animal = :id_animal')
            ->setParameter('id_animal', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getLastID()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.id_animal', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult()->getId_Animal();
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('u')
            ->where('u.something = :value')->setParameter('value', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}