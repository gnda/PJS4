<?php 
// src/DataFixtures/AppFixtures.php
namespace App\DataFixtures;

use App\Entity\Animal;
use App\Entity\Theme;
use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
		
		// Creation de l'administrateur
		$user1 = new Utilisateur();
		$user1->setIdUtilisateur(1);
		$user1->setToken(bin2hex(random_bytes(20)));
		$user1->setTypeUtilisateur(1);
		$user1->setUsername("user1");
		$user1->setEmail("chirac_vive_RPR@fondationchirac.eu");
		$user1->setPassword("1728");
		$user1->setPrenom("Jacques");
		$user1->setNom("Chirac");
		$user1->setAvatar("1.jpg");
		$user1->setAdresse("8 rue des Têtes de veaux 19000 Tulles");
		$user1->setEnabled(true);
		$user1->setDate_Inscription();
        $manager->persist($user1);
		
		// Creation de 2 autres utilisateurs
		$user2 = new Utilisateur();
		$user2->setTypeUtilisateur(2);
		$user2->setToken(bin2hex(random_bytes(20)));
		$user2->setUsername("user2");
		$user2->setEmail("pierre.dupont23@gmail.com");
		$user2->setPassword("5678");
		$user2->setPrenom("Pierre");
		$user2->setNom("Dupont");
        $user2->setAvatar("2.jpg");
		$user2->setAdresse("2 rue du pont 95200 Sarcelles");
		$user2->setEnabled(true);
		$user2->setDate_Inscription();
        $manager->persist($user2);
		
		$user3 = new Utilisateur();
		$user3->setTypeUtilisateur(3);
		$user3->setToken(bin2hex(random_bytes(20)));
		$user3->setUsername("user3");
		$user3->setEmail("mat.geo@gmail.com");
		$user3->setPassword("1234");
		$user3->setPrenom("Mathieu");
		$user3->setNom("Georges");
        $user3->setAvatar("3.jpg");
		$user3->setAdresse("9 rue Paris Descartes 75006 Paris");
		$user3->setEnabled(true);
		$user3->setDate_Inscription();
        $manager->persist($user3);

        $animal1 = new Animal();
        $animal1->setPrenom("Milou");
        $animal1->setEspece("Chien");
        $animal1->setRace("Race1");
        $animal1->setPhoto("1.jpg");
        $animal1->setAdresse("18 allée de fontainebleau 75019");
        $animal1->setDate_Inscription();
        $animal1->setEstAdopte(false);
        $manager->persist($animal1);

        $animal2 = new Animal();
        $animal2->setPrenom("Félix");
        $animal2->setEspece("Chat");
        $animal2->setRace("Race1");
        $animal2->setPhoto("2.jpg");
        $animal2->setAdresse("164 avenue jean jaurès 93500 pantin");
        $animal2->setDate_Inscription();
        $animal2->setEstAdopte(false);
        $manager->persist($animal2);

        $animal3 = new Animal();
        $animal3->setPrenom("Patrick");
        $animal3->setEspece("Iguane");
        $animal3->setRace("Race1");
        $animal3->setPhoto("3.jpg");
        $animal3->setAdresse("17 rue rivoli 75019 paris");
        $animal3->setDate_Inscription();
        $animal3->setEstAdopte(false);
        $manager->persist($animal3);
		
        $manager->flush();
    }
}