<?php
// src/Chat/ChatLoader.php
namespace App\Chat;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use App\Chat\Socket;

class ChatLoader{

    private $start = false;

    public function initIoServer(){

        $server = IoServer::factory(
            new HttpServer(new WsServer(new Socket())),
            8085
        );
        $server->run();
        $this->start = true;
    }

    public function isLoaded(){
        return $this->start;
    }
}