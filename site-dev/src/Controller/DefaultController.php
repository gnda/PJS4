<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;


class DefaultController extends AbstractController
{
    protected $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

	/**
     * @Route("/accueil", name="accueil")
	 * @Route("/", name="base")
     */
    public function accueil() : Response
    {
		return $this->render('accueil.html.twig');
    }

    /**
     * @Route("/compte", name="compte")
     */
    public function compte() : Response
    {
        return $this->render('compte.html.twig');
    }

    /**
     * @Route("/adoption", name="adoption")
     */
    public function adoption() : Response
    {
        return $this->render('adoption.html.twig');
    }

    /**
     * @Route("/rdv", name="rdv")
     */
    public function rendezVous() : Response
    {
        return $this->render('rdv.html.twig');
    }

    /**
     * @Route("/forum", name="forum")
     */
    public function forum() : Response
    {
        return $this->render('forum.html.twig');
    }

    /**
     * @Route("/faq", name="faq")
     */
    public function faq() : Response
    {
        return $this->render('faq.html.twig');
    }

    /**
     * @Route("/praticien", name="praticien")
     */
    public function praticien() : Response
    {
        return $this->render('praticien.html.twig');
    }
	
	/**
     * @Route("/erreur403", name="erreur403")
     */
    public function erreur() : Response
    {
		return $this->render('erreur_403.html.twig');
    }

    /**
     * @Route("/fiches_impl", name="fiches_impl")
     */
    public function fiches_impl() : Response
    {
        return $this->render('fiches_impl.html.twig');
    }

        /**
     * @Route("/secourisme", name="secourisme")
     */
    public function secourisme() : Response
    {
        return $this->render('secourisme.html.twig');
    }
}
