<?php
// src/Controller/SecurityController.php
namespace App\Controller;

use App\Form\LoginFormType;
use App\Form\SignUpFormType;
use App\Entity\Utilisateur;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    protected $em;
    protected $authUtils;
    protected $authChecker;
    protected $router;
    protected $jms;

    public function __construct(ObjectManager $em, AuthenticationUtils $authUtils, AuthorizationCheckerInterface $authChecker, RouterInterface $router, SerializerInterface $jms){
        $this->em = $em;
        $this->authUtils = $authUtils;
        $this->authChecker = $authChecker;
        $this->router = $router;
        $this->jms = $jms;
    }    
    /**
     * @Route("/login/{mode}/{token}/{pseudo}/{pass}", name="login"
     * , defaults={"mode" = null,"token" = null,"pseudo" = null,"pass" = null})
     */
	public function login(Request $request, $mode, $token, $pseudo, $pass)
	{
        // get the login error if there is one
        $error = $this->authUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastIdentifiant = $this->authUtils->getLastUsername();

        $utilisateur = new Utilisateur();

        $form = $this->createForm(LoginFormType::class, $utilisateur);
        $form->handleRequest($request);
        if($mode == "mobile"){
            $utilisateur->setUsername($pseudo);
            $utilisateur->setPassword($pass);
            $utilisateurAuth = $this->em->getRepository('App:Utilisateur')->loadUserByUsername($pseudo);
            if($utilisateurAuth->getPassword() == $pass){
                //$utilisateurAuth->setEnabled(true);
                //$this->em->persist($utilisateurAuth);
                //$this->em->flush();
                return new JsonResponse($this->jms->serialize($utilisateurAuth, 'json'));
            }
            else
                return new JsonResponse("", 'json');
        }else{
            if (($form->isSubmitted() && $form->isValid()) or $this->authChecker->isGranted('IS_AUTHENTICATED_FULLY'))
                return new RedirectResponse($this->router->generate('accueil'), 301);
        }

        return $this->render(
            'connexion.html.twig',
            array(
                'form' => $form->createView(),
                'error' => $error,
                'request' => $request,
            )
        );
	}

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request)
    {
        $request->getSession()->invalidate();
        $this->get('security.token_storage')->setToken();
        return new RedirectResponse($this->router->generate('accueil'), 301);
    }

    /**
     * @Route("/signup", name="signup")
     */
    public function signup(Request $request)
    {
        // get the signup error if there is one
        $error = $this->authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastIdentifiant = $this->authUtils->getLastUsername();

        $utilisateur = new Utilisateur();

        $form = $this->createForm(SignUpFormType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $utilisateur->getAvatar();

            $fileName = $utilisateur->getIdUtilisateur().'.'.$file->guessExtension();

            $file->move(
                $this->getParameter('avatars_directory'),
                $fileName
            );

            $utilisateur->setBrochure($fileName);

            return $this->redirect($this->generateUrl('accueil'));
        }

        return $this->render(
            'inscription.html.twig',
            array(
                'form' => $form->createView(),
                'error' => $error,
            )
        );
    }
}