<?php
// src/Controller/GestionController.php
namespace App\Controller;

use App\Entity\Animal;
use App\Entity\RendezVous;
use App\Form\AnimalFormType;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;

class GestionController extends AbstractController
{
    protected $em;
    protected $jms;

    public function __construct(ObjectManager $em, SerializerInterface $jms)
    {
        $this->em = $em;
        $this->jms = $jms;
    }

    /**
     * @Route("/animaux/{mode}", name="animaux", defaults={"mode" = null})
     */
    public function animaux(Request $request, RouterInterface $router, $mode) : Response
    {
        $animaux = $this->em->getRepository('App:Animal')->findAll();
        if($mode == 'mobile'){
            $json = $this->jms->serialize($animaux, 'json');
            return new Response($json);
        }
        else{
            $animal = new Animal();
            $form = $this->createForm(AnimalFormType::class, $animal);
            $form->handleRequest($request);
            $last_id = intval($this->em->getRepository("App:Animal")->getLastID());
            $last_id++;
            if ($form->isSubmitted() && $form->isValid()){
                $photo = $animal->getPhoto();
                $photo->move(
                    "uploads/animaux",
                    $last_id.".jpg"
                );
                $animal->setPhoto($last_id.".jpg");
                $animal->setDate_Inscription();
                $animal->setEstAdopte(false);
                $this->em->persist($animal);
                $this->em->flush();
                return new RedirectResponse($router->generate('animaux'), 301);
            }

            return $this->render('animaux.html.twig', array("form" => $form->createView(), "animaux" => $animaux));
        }
    }

    /**
     * @Route("/supprimer_animal/{mode}", name="supprimer_animal", defaults={"mode" = null})
     */
    public function supprimer_animal(Request $request, RouterInterface $router) : Response
    {
        $id = $request->request->get("id");
        $animal = $this->em->getRepository('App:Animal')->find($id);
        $this->em->remove($animal);
        $this->em->flush();
        return new Response("Suppression effectuée !");
        //return new RedirectResponse($router->generate('animaux'), 301);
    }

    /**
     * @Route("/ajouter_animal/{mode}", name="ajouter_animal", defaults={"mode" = null})
     */
    public function ajouter_animal(Request $request, RouterInterface $router) : Response
    {
        $animal = new Animal();
        $form = $this->createForm(AnimalFormType::class, $animal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($animal);
            $this->em->flush();
            return new Response("Ajout effectué");
        }
        //return $this->render('animaux.html.twig', array("form" => $form));
        return new RedirectResponse($router->generate('animaux'), 301);
    }

    /**
     * @Route("/rdv/{mode}", name="rdv", defaults={"mode" = null})
     */
    public function rendez_vous(Request $request, RouterInterface $router, TokenStorageInterface $token, $mode) : Response
    {
        if ($request->request->has("horaire") && $request->request->has("date")) {
            $horaire = $request->request->get("horaire");
            $date = $request->request->get("date");
            $rdv = new RendezVous();
            $rdv->setHoraire_Rdv($horaire);
            $rdv->setDate_Rdv($date);
            $rdv->setEstDisponible(true);
            $rdv->setId_Utilisateur($token->getToken()->getUser()->getIdUtilisateur());
            $this->em->persist($rdv);
            $this->em->flush();
            return new Response("succes");
        }
        return $this->render('rdv.html.twig', array("rdvs" => $this->em->getRepository("App:RendezVous")->findAll()));
    }
}