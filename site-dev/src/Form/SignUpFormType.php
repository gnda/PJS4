<?php
// src/Form/SignUpFormType.php
namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SignUpFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_type_utilisateur', RadioType::class, array('mapped' => false, 'label' => 'Je suis :'))
            ->add('_nom', TextType::class, array('label' => 'Nom :'))
            ->add('_prenom', TextType::class, array('label' => 'Prénom :'))
            ->add('_email', EmailType::class, array('label' => 'E-mail :'))
            ->add('_username', TextType::class, array('label' => 'Pseudo :'))
            ->add('_avatar', FileType::class, array('label' => 'Votre avatar'))
			->add('_password', PasswordType::class, array('label' => 'Mot de passe :'))
            ->add('conf_pass', PasswordType::class, array('mapped' => false, 'label' => 'Confirmez le mot de passe :'))
			->add('signup', SubmitType::class, array('label' => "Confirmer l'inscription"))
        ;
    }
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => Utilisateur::class,
		));
	}
}