<?php
// src/Form/LoginFormType.php
namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LoginFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', TextType::class, array('label' => 'Pseudo :'))
			->add('_password', PasswordType::class, array('label' => 'Mot de passe :'))
			->add('keep_connection', CheckboxType::class, array('mapped' => false, 'required' => false, 'label' => 'Maintenir la connexion'))
			->add('connect', SubmitType::class, array('label' => 'Se connecter'))
        ;
    }

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => Utilisateur::class,
		));
	}
}