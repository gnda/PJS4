<?php
// src/Form/AnimalFormType.php
namespace App\Form;

use App\Entity\Animal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AnimalFormType extends AbstractType
{
    private $router;

    public function __construct(RouterInterface $router){
        $this->router = $router;
    }

    public function buildForm( FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('_race', RadioType::class, array('mapped' => false, 'label' => 'Je suis :'))
            ->add('_photo', FileType::class, array('label' => 'Photo'))
            ->add('_prenom', TextType::class, array('label' => 'Prénom :'))
            ->add('_espece', TextType::class, array('label' => 'Espèce :'))
            ->add('_race', TextType::class, array('label' => 'Race :'))
            ->add('_adresse', TextType::class, array('label' => 'Adresse :'))
			->add('confAddAnimal', SubmitType::class, array('label' => "Ajouter l'animal"))
            ->setAction($this->router->generate('animaux'))
            ->setMethod('POST');
    }
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => Animal::class,
		));
	}
}