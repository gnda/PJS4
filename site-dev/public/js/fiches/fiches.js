var modals = document.getElementsByClassName("modal");

// When the user clicks the button, open the modal 
$(function(){

	$("#dogs_fiches .apercu_fiche").each(function(){
	var indice = $(this).index();

     $(this).click(
     	function(){
     		$("#modals_chien .modal:eq("+indice+")").css("display", "block");
     	});
   
	});
	$("#cats_fiches .apercu_fiche").each(function(index){
		$(this).off();
     $(this).click(
     	function(){
     		$("#modals_chat .modal:eq("+index+")").css("display", "block");
     	});
	});	
	$("#birds_fiches .apercu_fiche").each(function(index){

     	$(this).click(
     	function(){
     		$("#modals_oiseau .modal:eq("+index+")").css("display", "block");
     	});
	});	
	$("#rondents_fiches .apercu_fiche").each(function(index){

     $(this).click(
     	function(){
     		$("#modals_rongeur .modal:eq("+index+")").css("display", "block");
     	});
	});
	$("#fishes_fiches .apercu_fiche").each(function(index){

     $(this).click(
     	function(){
     		$("#modals_poisson .modal:eq("+index+")").css("display", "block");
     	});
	});	
});

// When the user clicks on <span> (x), close the modal
$(".close").on("click", function() {
    $(".modal").css("display", "none");
});

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event)
{
	for(modal of modals)
	{
	if(event.target == modal)
		modal.style.display = "none";
	}
}