/*.addEventListener("click",function()
{
    document.getElementById("txtvote").innerHTML = "Merci pour votre vote !";
})*/

var tabImage = [];

tabImage.push("./img/accueil/chat.jpg");
tabImage.push("./img/accueil/chat2.jpeg");
tabImage.push("./img/accueil/hamster.jpg");
tabImage.push("./img/accueil/lapin.jpeg");
tabImage.push("./img/accueil/perroquet.jpg");
tabImage.push("./img/accueil/tortue.jpeg");

var tableauClasses = document.querySelectorAll("#images > div");
var indicesSortis = [];
function setImage(container,imageUrl)
{
	container.style.background = "url("+imageUrl+") no-repeat";
	container.style.height = "15vw";
	container.style.width = "22vw";
	container.style.backgroundSize = "contain";
}

function displayImage()
{
	var randomInt = 0;
	for(var i = 0; i<tableauClasses.length;i++)
	{
		randomInt = Math.floor((Math.random() * tabImage.length));
		while(!indice(randomInt))
		{
			randomInt = Math.floor((Math.random() * tabImage.length));
		}
		indicesSortis.push(randomInt);
		setImage(tableauClasses[i],tabImage[randomInt]);
	}
}

function indice(integer)
{
	for(nombre of indicesSortis)
	{
		if(integer == nombre)
			return false;
	}
	return true;
}



function displayNoneAll(tableau)
{
    for(imgTab of tableau)
        imgTab.style.display="none";
}

window.onload = function()
{
	
	displayImage();
	for(var i=0; i<tableauClasses.length;i++)
	{
		tableauClasses[i].addEventListener("click",function()
		{
			displayNoneAll(tableauClasses);
			document.getElementById("txtvote").innerHTML = "Merci pour votre vote !";
		},false)
	}
}