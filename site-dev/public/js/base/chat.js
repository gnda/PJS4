var bouton = document.getElementById("submitComment");
var chatbox = document.getElementById("chatbox");
var commentaire = document.getElementById("comment_txt");
var utilisateur = document.getElementById("utilisateur").textContent;

var couleurs = ['Black','Teal','Green','MidnightBlue','RebeccaPurple','SlateBlue','Maroon','Crimson','Purple','DarkRed','SaddleBrown','FireBrick','DarkGoldenRod','Salmon','Orange','Gold','Magenta','Red','DarkMagenta'];


var websocket = new WebSocket("ws://localhost:8085/");

$(function(){
{
    chatbox.innerHTML += "<p style='color:grey;margin-left:0.75rem;font-size:80%'><i>Vous êtes connecté au chat</i></p>";
    var couleurCommentaire1 = couleurs[Math.floor(Math.random() * couleurs.length)];
    var couleurCommentaire2 = couleurs[Math.floor(Math.random() * couleurs.length)];

    bouton.addEventListener("click",function(){
        if(commentaire.value.length > 0)
        {
            websocket.send(
                JSON.stringify({
                    "type":"submit",
                    "username":utilisateur,
                    "msg":commentaire.value
                })
            );
            commentaire.value = null;
        }
    });

    commentaire.addEventListener('keypress',function(e)
    {
        if(e.keyCode == 13 && !(!this.value.replace(/\s/g, '').length))
        {
            bouton.click();
        }
    })

    websocket.open = function(e)
    {
        console.log("Initialisation du chat");
    }

    websocket.onerror = function(e)
    {
        console.log("Échec du lancement");
    }

    websocket.onmessage = function(e)
    {
        var json = JSON.parse(e.data);
        switch(json.type)
        {
            case "submit":
                if(json.user == utilisateur)
                    chatbox.innerHTML += "<p class='commentChatBox' title='"+formattedDate()+"' style='color:"+couleurCommentaire1+"'><strong>"+json.user+" : </strong> "+json.text+"</p>";
                else
                    chatbox.innerHTML += "<p class='commentChatBox' title='"+formattedDate()+"' style='color:"+couleurCommentaire2+"'><strong>"+json.user+" : </strong> "+json.text+"</p>";
                chatbox.scrollTo(0,chatbox.scrollHeight);
                break;

        }
    }

    $("#disconnect").click(function()
    {
        window.location.href = "index.php?action=disconnect";
    })

}});

function formattedDate(d = new Date) {
    let month = String(d.getMonth() + 1);
    let day = String(d.getDate());
    const year = String(d.getFullYear());

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return `${month}/${day}/${year}` +" "+ d.getHours()+":"+d.getMinutes();
}