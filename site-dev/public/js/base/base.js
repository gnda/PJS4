function overridePostAjaxBouton(nomBtn, action, type, elt, callback){
    overridePostBouton(nomBtn, function(){
        $.post(action,((type == "form" ? $(elt).serialize() : elt))).done(function(data){
        if(callback !== null)
            callback($.parseJSON(data));
        });
    });
}

function overridePostBouton(nomBtn, callback){
    if($(nomBtn).length){
        $(nomBtn).bind('click', function(e){
            e.preventDefault();
            callback();
            return false;
        });
    }
}

function traiterRetour(data){
    if(data.keys == null)
        alert(data);
}

function getScrollPercent(){
    var h = document.documentElement,
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';
    return (h[st]||b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100;
}

function scrollAuto()
{
    if(window.innerWidth > 768)
    {
        $(window).on('scroll',function(){
           if(getScrollPercent() == 0)
           {
                $('nav').css("transform","none");
           }
           else
                $('nav').css('transform','translateY(-145px)');
        });
    }
    else
    {

    }
        
}

// $(function(){
//     scrollAuto();        
// });