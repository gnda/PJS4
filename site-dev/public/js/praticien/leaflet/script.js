var marqueurs = [];
var recherche = "";
var champVille = document.getElementById("ville");
var champAdresse = document.getElementById("start");
var mymap = L.map('map').setView([48.8566101, 2.3514992], 11);
L.tileLayer('https://api.mapbox.com/styles/v1/ii02735/cjcs9lhj16uyi2sqnp8dafs7e/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiaWkwMjczNSIsImEiOiJjamNyZ3V2cTIyY3o2MnZvNG1pZmxua20wIn0.dDefu45uaH2oRz73y0FRlg', {
}).addTo(mymap);


var pawIcon = L.icon({
	iconUrl: './img/praticien/leaflet/paw.png',
	iconSize: [19,15.75]
});




var control = new L.Routing.Control({
    language: 'fr'
}).addTo(mymap);
	


//Importation dynamique des données géographiques en JSON 
var getJSON = function(callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.open('GET', "https://nominatim.openstreetmap.org/search.php?q="+recherche+"&polygon_geojson=1&viewbox=&limit=500&format=json", true);
    xhttp.responseType = 'json';
    xhttp.onload = function() {
      if (xhttp.status === 200) {
        callback(null, xhttp.response);
      } else {
        callback(status, xhttp.response);
      }
    };
    xhttp.send();
};


//ajout d'icônes depuis geoJSON


document.querySelectorAll("input[type='submit']")[0].addEventListener("click",function()
{
  recherche = champAdresse.value;
  getJSON(function(err, data) {
  if (err !== null) 
    alert('Something went wrong: ' + err);
  else
  	control.spliceWaypoints(0, 1, [data[0].lat,data[0].lon]);
  }
)},false);


document.querySelectorAll("input[type='submit']")[1].addEventListener("click",function()
{
  recherche = "vétérinaires+"+champVille.value;
  //on supprime les marqueurs de la recherche précédente
  if(marqueurs.length > 0)
  {
    for(var i = 0; i<marqueurs.length;i++)
    {
      mymap.removeLayer(marqueurs[i]);

    }
    //On n'oublie pas de VIDER la LISTE !!!!
    marqueurs = [];
  }
	
  getJSON(function(err, data) {
  if (err !== null) {
    alert('Something went wrong: ' + err);
  } else 
  {
    for(var i=0;i<data.length;i++)
    {
        marqueurs.push(new L.Marker([data[i].lat,data[i].lon],{icon:pawIcon}));
        mymap.addLayer(marqueurs[i]);
        marqueurs[i].bindPopup(data[i].display_name.replace("France métropolitaine,",""));
        //On efface les précédents marqueurs
	     	marqueurs[i].addEventListener('click', function(e) {

		        control.spliceWaypoints(control.getWaypoints().length - 1, 1, e.latlng);

		});

    }
    console.log(marqueurs);
  }
});

},false);


document.getElementById("clear").addEventListener("click",function(){
	control.getPlan().setWaypoints([]);
},false);

	
