﻿var q; //référence à  un bloc div pour l'affichage (<div "id=QUEST"></div>)
var timer;  //variable référençant un objet temporisateur
var  temps_imparti =  10000;  //temps imparti pour donner la réponse (10s)

var somScore=0;
var invite = "Vous avez une minute pour r\351pondre ";
	invite += "apr\350s avoir d\351marr\351 le test <br/><br/>";
	invite += "<a href='' class='button1' onclick='question(temps_imparti); return false;'>d\351marrer</a>";

var quests;

var i;
var quest;
var repOK = "Bonne r\351ponse !";
var repNOK = "'D\351sol\351, la r\351ponse est incorrecte.'";
var repNO = "d\351sol\351 le temps imparti est d\351pass\351 ";
var fin = "Fin du test, votre score est de : ";


window.onload=function(){ 
	init(); 
};
	
function init(){
	//initialisation de q
	q = document.getElementById("QUEST");
	
	//lance le jeu pour la première fois
	lancer();
}

function lancer() {
	quests = [
	{question :"Quel est le meilleur animal pour vous ? ", 
	choix: ["L’animal est destiné à mon enfant qui à moins de six ans, j’envisage un oiseau", "J’envisage un chat car je souhaite un animal indépendant, mais qui à de tout de même besoin d’attention", "J’envisage un chien car j’ai très peu de temps à lui accorder"], 
	reponse : 1},
	{question :"Êtes-vous en mesure de subvenir à ses besoins ?", 
	choix: ["J’ai déjà prévu un budget mensuel pour les soins, la nourriture, et un budget initial d’environ 200€ pour l’adoption", "Il mangera la même nourriture que moi", "Les croquettes ou graine ne coûte pas cher de toute façons"], 
	reponse : 0},
	{question : "Êtes-vous prêt à assumer vos responsabilités ?",
	choix : ["Je suis prêt(e) à renouveler l’eau , la nourriture et nettoyer l’environnement de mon animal toutes les semaines", "Je suis patient(e) et je compte me dévouer à la prise en charge de mon animal", "Quand je m’absente plus d’un jour j’alterne parfois je prévois un gardien pour s’occuper de mon animal parfois, je lui laisse de la liberté en le laissant sans gardien"],
	reponse : 1},
	{question : "Avez-vous l’habitation qu’il faut ?",
	choix : ["Je vis avec mes grand parent qui ont du mal à supporter le bruit, et je souhaite un perroquet ", "Je vis dans un appartement et j’envisage d’adopter un Saint-Bernard (chien)", "J’ai un grand jardin et je veux adopter un chien"],
	reponse : 2}
	];

	//affichage de l'invite
	q.innerHTML = invite;
}

function question ()  {
	//affichage de la question dans le bloc QUEST
	
	//i = Math.floor(Math.random()*quests.length);
	i = 0;
	
	//initialise i déclaré en global (chaque fonction aura la même valeur)
	// valeur aléatoire mémorisée dans i
	quest  =   "<h3 align='center'> QUESTION " + i + " :</h3><hr>"; 
	quest  +=   quests[i].question; // stockage de la question 
	quest  +=	"<ul style='list-style-type:none'>";
	for(j in quests[i].choix) // stockage des réponses 
		quest  +=   "<li><input  type='radio' name='rd' value='" + j +"' onclick='reponse(this.value)' />" + quests[i].choix[j] + "</li>";
	quest  +=     "</ul>";
	q.innerHTML = quest; // affichage de quest sur la page HTML
	
	timer= setTimeout("abandon()", temps_imparti);  //définit "timer" pour lancer abandon() après le temps imparti	
	
	 
}

function reponse(rep)  { 
	
	
	clearTimeout(timer); 	//stoppe le timer
	//analyse de la réponse cliquée et suivant le cas
	if(i != quests.length - 1)
	{
		if(rep != quests[i].reponse)
		{	
			alert(repNOK);
		}
		else {
			alert(repOK);
			somScore=somScore+1;
		}	
		//Relancer le jeu
		questionSuivante();
	}
	else
	{		
		if(rep != quests[i].reponse)
		{	
			alert(repNOK);
		}
		else {
			alert(repOK); 
			somScore=somScore+1;
		}
		$('#QUEST').append("<p align='center'>Fin du test, votre score sur 4 est de : " + somScore + " :</p><hr>");
		
	
		
	}
	
}

function questionSuivante(){
	i+=1;
	quest  =   "<h3 align='center'> QUESTION " + i + " :</h3><hr>"; 
	quest  +=   quests[i].question; // stockage de la question 
	quest  +=	"<ul style='list-style-type:none'>";
	for(j in quests[i].choix)// stockage des réponses 
		quest  +=   "<li><input  type='radio' name='rd' value='" + j +"' onclick='reponse(this.value)' />" + quests[i].choix[j] + "</li>";
	quest  +=     "</ul>";
	q.innerHTML = quest; // affichage de quest sur la page HTML
	
	timer= setTimeout("abandon()", temps_imparti);  //définit "timer" pour lancer abandon() après le temps imparti
	
}

function abandon() {	
	//message d'alert repNO indiquant l'abandon
	//Relancer le jeu
	alert(repNO);
	lancer();
}

	



