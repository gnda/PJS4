# Chat PJS4

Il s'agit *d'une approche de chat*, il reste encore des points à améliorer :

* Le lancement du serveur **marche**, **aucun script n'est à exécuter** depuis un terminal.
    On lance le serveur / ouvre le port depuis une requête AJAX : **$.get("xhttp.open("GET", "./server_Sockets/server/websocket_server.php",true);)"**

    Lancer le script en AJAX rend ce dernier *asynchrone* : le serveur tourne en continue en arrière-plan.
    Le mettre directement dans un contrôleur PHP **risquerait de bloquer le serveur**.

    **FAIRE ATTENTION AUX CHEMINS QUI SONT RELATIFS DANS LE SCRIPT DES WEBSOCKETS**

* Pour régler le problème d'écoute, il faut utiliser **la base de données** :
    
    1. On crée un chat dans la table CHAT lorsque **l'utilisateur souhaite démarrer une discussion**. 
       On doit également prendre en compte les utilisateurs qui sont connectés.
    2. S'il n'y a aucun autre utilisateur, on ferme la connexion pour le chat : **on économisera les ressources ainsi**
    3. Pour détecter s'il y a un changement, il faudra soit employer un **rafraîchissement AJAX** ou employer **les websockets** qui exécuteront une requête SQL qui différenciera le booléen de connexion (si un utilisateur est toujours sur le chat le booléen vaudra 1 sinon 0)...

Mettre en place **la socket maître** qui se chargera de récupérer les identifiants des utilisateurs connectés (on ne passe pas par la variable $_SESSION["utilisateur"])

