<?php

function formAuth()
{
	header('Location:login.php');
}

function connexion()
{
	$login = isset($_POST['nom']) && !empty($_POST['nom']) ? $_POST['nom'] : null;
	$mdp = isset($_POST['password']) && !empty($_POST['password']) ? $_POST['password'] : null;
	if(is_null($login) || is_null($mdp))
	{
		$message = "Veuillez renseigner tous les champs";
		require('login.php');
	}
	else
	{
		require('modele.php');
		$id = getUser($login,$mdp);
		if(isset($id) AND !empty($id))
		{
			connecter($id);
			$_SESSION['utilisateur'] = array("id"=>$id,"login"=>$login);
			require_once("chat.php");	
		}
		else
		{
			$message = "Mot de passe / identifiant invalide";
			require('login.php');
		}
	}	
}

function disconnect()
{
	require('modele.php');
	deconnecter($_SESSION['utilisateur']["id"]);
	session_destroy();
	formAuth();
}