
<!DOCTYPE html>
<html lang="fr-FR">
<head>
	<title>Test chat</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="style_chat.css">
</head>
<body>
	<div id="utilisateur" style="display: none"><?php echo $_SESSION["utilisateur"]["login"]; ?></div>
	<section>
	<div id="chatbox">
	</div>
	<div id="commands">
	<input type="text" placeholder="Tapez votre commentaire..." id="comment_txt">
	<button id="submit">Soumettre</button>
	</div>
	</section>
	<script src="js/jquery.js"></script>
	<script src="js/script.js"></script>
</body>
</html>