<?php
set_time_limit(0);

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
require_once('../vendor/autoload.php');

class Socket implements MessageComponentInterface {
	protected $clients;
	protected $users;
	protected $SQLInstance;
	private $idPartie;
	public function __construct() {

		$this->clients = new \SplObjectStorage;
		$this->idPartie = 0;
	}

	public function onOpen(ConnectionInterface $conn) {
		$this->clients->attach($conn);
		// $this->users[$conn->resourceId] = $conn;
	}

	public function onClose(ConnectionInterface $conn) {
		$this->clients->detach($conn);
		// unset($this->users[$conn->resourceId]);
	}

	public function onMessage(ConnectionInterface $from,  $data) {
		$from_id = $from->resourceId;
		$data = json_decode($data);
		$type = $data->type;
			switch ($type) {
		
				case 'submit':
					$user_id = $data->username;//On récupère la variable session de l'émetteur
					$message = $data->msg;
					foreach ($this->clients as $client) {
						$client->send(json_encode(array("type"=>$type,"user"=>$user_id,"text"=>$message)));
					}
				break;
		}
	}

	public function onError(ConnectionInterface $conn, \Exception $e) {
		$conn->close();
	}
}
$server = IoServer::factory(
	new HttpServer(new WsServer(new Socket())),
	8085
);
$server->run();
?>
