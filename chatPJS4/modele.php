<?php

$bdd = null;

function init()
{
	if(!isset($bdd))
	{
		try{
		
		$bdd = new PDO("mysql:host=localhost;dbname=testChat","root","root");
		$bdd->exec("SET CHARACTER SET utf8");
		
		}
		catch(Execption $e)
		{
			die("Erreur : ".$e->getMessage());
		}
		return $bdd;
	}
}

function getUser($login,$mdp)
{
	$query = init()->prepare("SELECT * FROM utilisateur WHERE login=:login AND password=:password");
	$query->execute(array(':login'=>$login,':password'=>$mdp));
	return $query->fetch()["Id"];
}

function connecter($id)
{
	$query = init()->prepare("UPDATE utilisateur set estConnecte=1 WHERE id=$id");
	$query->execute();
}

function deconnecter($id)
{
	$query = init()->prepare("UPDATE utilisateur set estConnecte=0 WHERE id=$id");
	$query->execute();
}
