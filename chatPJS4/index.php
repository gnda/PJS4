<?php 

if(session_status() == PHP_SESSION_NONE){
	session_start();
}
		
if(isset($_SESSION['utilisateur']) AND count($_SESSION['utilisateur']) > 0)
{
	require('chat.php');	
}

if((count($_GET)!=0) && !(isset($_GET['action']))){
	die("Page introuvable");
}
	
else{
	if(count($_GET)==0){
		$action="formAuth";	
	}
	
	if (isset($_GET['action'])) {
		$action= $_GET['action'];
	}
		
	//inclure le fichier php de contrôle 
	//et lancer la fonction-action issue de ce fichier.	
	require ('controleur.php');   
	$action(); 
}	


