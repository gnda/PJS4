<?php  
function connect_pdo(){
	$hostnameDB = 'localhost';
	$usernameDB = 'root';
	$passwordDB = 'root';
	$nameDB = 'pjs4-rdv';
	try
	{
		$pdo = new PDO("mysql:host=$hostnameDB;dbname=$nameDB", $usernameDB, $passwordDB, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
		return $pdo;
	}
	catch(Exception $e)
	{
		die("Echec : " . $e->getMessage());
	}
}

?>

